# Express Code in TypeScript

### RUN DEV ENVIRONMENT (After done the installation guide)

```
    docker compose up
```

## INSTALLATION GUIDE - DEV Environment

### Prequisite

1.  Use must have Docker Deamon Running in your machine
2.  Have yarn install in your machine
3.  Installation Step
    a. Clone the project
    b. Go the the downloaded local repo
    c. Run
    ```
        yarn install
    ```
    d. RUN
    ```
        docker compose up --build
    ```
    e. Check that the server is running: Access the URL: localhost

### EXTENSE LIB TYPES

1. Define the types in a <name>.d.ts file
2. Provide the path to that file in the tsconfig: in side the "include key"

_IMPORTANT NODE_ 1. By default: ts-node does not read the "files, include, exclude". To force ts-node to read these keys in ts-config.

1. Add the "--files" tag: `E.g. ts-node --files <file_path>`
2. More information: https://www.npmjs.com/package/ts-node | Search '--files'
