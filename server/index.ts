import express, { Express, Request, Response } from "express"
import dotenv from 'dotenv';
import { attachABody } from "./middlewares/authenticate";


dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.get("/", attachABody, (req: Request, res: Response) => {
  // console.log();
  res.send(req.user);
});

app.listen(port, () => {
  console.log(`[SERVER]: Server is running at httpL//localhost:${port}`);
});
