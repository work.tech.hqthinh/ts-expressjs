import { Response, Request, NextFunction } from "express"

type Body = {
    id: number,
    user: string,
    code: string,
}

const attachABody = function (req: Request, res: Response, next: NextFunction) {

    const body: Body = {
        id: 1,
        user: 'Huỳnh Quốc Thịnh',
        code: 'HQTH'
    }
    req.user = body;
    process.env.body = body;
    next();
}

export { attachABody }