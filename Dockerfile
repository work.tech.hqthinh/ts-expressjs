FROM node:19
WORKDIR /app

COPY package.json yarn.lock tsconfig.json nodemon.json /app/
RUN yarn global add typescript@4.9.5 tslib@2.5.0 ts-node@10.9.1 nodemon@2.0.20