namespace NodeJS {
    interface ProcessEnv {
        body: {
            id: number,
            user: string,
            code: string,
        }
    }
}