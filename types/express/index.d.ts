module Express {
    interface Request {
        user: {
            id: number,
            user: string,
            code: string,
        }
    }
}